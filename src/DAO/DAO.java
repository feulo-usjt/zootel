/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;

/**
 *
 * @author feulo
 * @param <T>
 */
public interface DAO<T>{
    
    public ArrayList<T> carregarTodos();
    public T carregar(int cod);
    public void alterar(T t);
    public void excluir(T t);
    public void inserir(T t);
    
}
