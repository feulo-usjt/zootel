/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author feulo
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory{

	public static Connection getConnection() throws SQLException{
		String url = "jdbc:mysql://localhost/zootel"; // colocar url do BD
		Properties prop = new Properties();
		prop.setProperty("user", "root"); //Colocar usuário do BD
		prop.setProperty("password", "password"); // Colocar Senha do BD
		prop.setProperty("useSSL", "false");
		prop.setProperty("autoReconnect", "true");
		
		return DriverManager.getConnection(url, prop);
	   }

}

